package com.network;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.jama.Matrix;

public class Network {

	private int[] size;
	private int layernum;
	private Random random = new Random();

	public ArrayList<double[]> biases = new ArrayList<>();

	public ArrayList<double[][]> weights = new ArrayList<>();

	public float eta = 0.5f;

	public Object[] results = new Object[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public Network(ArrayList<double[][]> w, ArrayList<double[]> b, int[] sizes, Object[] presult) {
		weights = w;
		biases = b;
		layernum = size.length;
		size = sizes;
		results = presult;
	}

	public Network(int[] sizes,Object[] presult) {
		size = sizes;
		results = presult;
		layernum = size.length;
		// Generate bias
		for (int i = 1; i < size.length; i++) {
			double[] bList = new double[size[i]];
			for (int j = 1; j < size[i]; j++) {
				bList[j] = random.nextGaussian();
			}
			biases.add(bList);
		}

		for (int i = 1; i < size.length; i++) {
			ArrayList<double[]> wList = new ArrayList<double[]>();
			for (int j = 0; j < size[i - 1]; j++) {
				double[] w = new double[size[i]];
				for (int z = 0; z < size[i]; z++) {
					w[z] = random.nextGaussian();
				}
				wList.add(w);
			}
			weights.add(wList.toArray(new double[0][]));
		}
	}

	public double[] feedforward(double[] matrix) {
		Matrix a = new Matrix(new double[][] { matrix });
		for (int i = 0; i < weights.size(); i++) {
			Matrix w = new Matrix(weights.get(i));
			a = a.times(w).plus(new Matrix(new double[][] { biases.get(i) })).Sigmoid();
		}
		return a.getArray()[0];
	}

	public void SDG(ArrayList<trainData> train_data, int train_times, int mini_batch_size, float eta) {
		this.eta = eta;
		for (int i = 0; i < train_times; i++) {
			ArrayList<?> shuffled = (ArrayList<?>) train_data.clone();
			Collections.shuffle(shuffled);
			ArrayList<trainData> mini_batches = new ArrayList<trainData>();
			for (int j = 0; j < shuffled.size(); j++) {
				mini_batches.add((trainData) shuffled.get(j));
				if (mini_batches.size() == mini_batch_size) {
					update_minibatches(mini_batches);
					mini_batches = new ArrayList<trainData>();
					System.out.println("Processing Batches" + ((j + 1) / mini_batch_size) + "/"
					        + (shuffled.size() / mini_batch_size));
				}
			}
		}
	}

	public void update_minibatches(ArrayList<trainData> minibatches) {
		ArrayList<double[][]> nable_biases = new ArrayList<double[][]>();
		ArrayList<double[][][]> nable_weights = new ArrayList<double[][][]>();
		for (int i = 0; i < minibatches.size(); i++) {
			Result result = backProp(minibatches.get(i).data, minibatches.get(i).result);
			nable_biases.add(result.nable_biases);
			nable_weights.add(result.nable_weight);
		}

		for (int l = 0; l < biases.size(); l++) {
			for (int i = 0; i < biases.get(l).length; i++) {
				double nable_bias = 0;
				for (int j = 0; j < nable_biases.size(); j++) {
					nable_bias += nable_biases.get(j)[l][i];
				}
				//System.out.println(nable_bias); // The most of them are 0.0 because of matrix problem
				biases.get(l)[i] -= (eta / nable_biases.size()) * nable_bias;
			}
		}

		for (int l = 0; l < weights.size(); l++) {
			for (int i = 0; i < weights.get(l).length; i++) {
				for (int j = 0; j < weights.get(l)[i].length; j++) {
					double nable_weight = 0;
					for (int z = 0; z < nable_weights.size(); z++) {
						nable_weight += nable_weights.get(z)[l][i][j];
					}
					//System.out.println(nable_weight);
					weights.get(l)[i][j] -= (eta / nable_weights.size()) * nable_weight;
				}
			}
		}
	}

	public Result backProp(double[] data, Object result) {
		ArrayList<double[]> delta_biases = new ArrayList<>();
		ArrayList<double[][]> delta_weights = new ArrayList<>();

		Matrix a = new Matrix(new double[][] { data });
		ArrayList<double[]> activations = new ArrayList<double[]>();
		ArrayList<double[]> zs = new ArrayList<double[]>();
		activations.add(data);
		for (int i = 0; i < weights.size(); i++) {
			Matrix w = new Matrix(weights.get(i));
			a = a.times(w).plus(new Matrix(new double[][] { biases.get(i) }));
			zs.add(a.getArray()[0]); // zs is massive big that Sigmoid Fuction can handled
			a = a.Sigmoid();
			activations.add(a.getArray()[0]);
		}

		Matrix delta = new Matrix(new double[][] { cost_derivative(activations.get(activations.size() - 1), result) })
		        .dot(new Matrix(new double[][] { sigmoid_derivative(zs.get(zs.size() - 1)) })); // Potential problem
		delta_biases.add(0, delta.getArray()[0]);
		delta_weights.add(0, new Matrix(new double[][] { activations.get(activations.size() - 2) }).transpose()
		        .times(delta).getArray());
		
		for (int i = 2; i < layernum; i++) {
			double[] sp = sigmoid_derivative(zs.get(zs.size() - i));
			delta = delta
			        .times(new Matrix(weights.get(weights.size() - i + 1)).transpose())
			        .dot(new Matrix(new double[][] { sp })); // Potential problem
			delta_biases.add(0, delta.getArray()[0]);
			delta_weights.add(0, new Matrix(new double[][] { activations.get(activations.size() - i - 1) }).transpose()
			        .times(delta).getArray());
		}
		return new Result(delta_weights.toArray(new double[0][][]), delta_biases.toArray(new double[0][]));
	}

	public Object test(double[] test_data) {
		double[] result = feedforward(test_data);
		int max_index = -1;
		double max = 0;
		for (int i = 0; i < result.length; i++) {
			if (result[i] > max) {
				max = result[i];
				max_index = i;
			}
		}
		return results[max_index];
	}

	private double[] cost_derivative(double[] activations, Object result) {
		double[] delta = new double[activations.length];
		for (int i = 0; i < activations.length; i++) {
			delta[i] = (activations[i] - (result == results[i] ? 1 : 0));
		}
		return delta;
	}

	private double[] sigmoid_derivative(double[] z) { // Problem is here
		double[] sigmoid_z = new double[z.length];
		for (int i = 0; i < z.length; i++) {
			sigmoid_z[i] = sigmoid_prime(z[i]);
		}
		return sigmoid_z;
	}

	private double sigmoid(double z) {
		return 1 / (1 + Math.pow(Math.E, -z));
	}

	private double sigmoid_prime(double z) {
		return sigmoid(z) * (1 - sigmoid(z));
	}

	public void saveMemory(String filePath) throws IOException {
		File mem = new File(filePath);
		if (mem.isDirectory()) {
			throw new IOException("Target file cann't be a directory");
		}
		mem.mkdirs();

		FileOutputStream os = new FileOutputStream(mem);
		ObjectOutputStream oos = new ObjectOutputStream(os);
		oos.writeObject(new Memory(weights, biases));
		oos.close();
		os.close();
	}

	public void readMemory(String filePath) throws IOException {
		File mem = new File(filePath);
		FileInputStream is = new FileInputStream(mem);
		ObjectInputStream ois = new ObjectInputStream(is);
		try {
			Memory memory = (Memory) ois.readObject();
			weights = memory.weights;
			biases = memory.biases;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ois.close();
		is.close();
	}

	/*private void PrintDoubleArrays(List<double[]> arrays) {
		System.out.println("[");
		double[] objects = arrays.get(0);
		for (double object : objects) {
			System.out.println(object);
		}
		System.out.println("]");
	}*/
}

class trainData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7400404719715328826L;

	public double[] data;
	public int result;

	public trainData(double[] d, int r) {
		result = r;
		data = d;
	}

}

class Memory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5968361251707635055L;

	public ArrayList<double[]> biases = new ArrayList<>();
	public ArrayList<double[][]> weights = new ArrayList<>();

	public Memory(ArrayList<double[][]> weight, ArrayList<double[]> bias) {
		biases = bias;
		weights = weight;
	}
}

class Result {
	public double[][] nable_biases;
	public double[][][] nable_weight;

	public Result(double[][][] w, double[][] b) {
		nable_biases = b;
		nable_weight = w;
	}
}