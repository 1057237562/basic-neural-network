package com.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Main {

	public static Network current = new Network(new int[] { 784, 16, 16, 10 },
	        new Object[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

	public static void main(String[] arg0) {
		String[] arg = arg0[0].split(" ");
		if (arg[0].contains("load")) {
			if (arg.length == 3) {
				decodeJavaPackage(arg[1], 50000, arg[2]);
			} else {
				decodeJavaPackage(".\\mnist.jpkl", 50000, ".\\mnist.ase");
			}
			return;
		}
		if (arg[0].contains("train")) {
			if (arg.length == 2) {
				training(arg[1]);
			} else {
				System.out.println("starting training");
				training(".\\mnist.ase");
			}
		}

	}

	public static void training(String filepath) {
		try {
			FileInputStream is = new FileInputStream(new File(filepath));
			ObjectInputStream ois = new ObjectInputStream(is);
			try {
				ArrayList<trainData> trainDatas = (ArrayList<trainData>) ois.readObject();
				current.SDG(trainDatas, 1, 10, 0.5f);
				Object result = current.test(trainDatas.get(1).data);
				System.out.println("Network response is:" + result + ",Correct answer is:" + trainDatas.get(1).result);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ois.close();
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void decodeJavaPackage(String filepath, int datacount, String savePath) {
		ArrayList<trainData> datas = new ArrayList<trainData>();
		File data = new File(filepath);
		try {
			BufferedReader reader = new BufferedReader(new FileReader(data));
			String str = null;
			String combine = null;
			while ((str = reader.readLine()) != null) {
				if (str.contains(",")) {
					combine += str;
					String[] dataset = combine.split("\\[");
					dataset = dataset[dataset.length - 1].split("\\]");
					String matrix = dataset[0];
					while (matrix.contains("  ")) {
						matrix = matrix.replaceAll("  ", " ");
					}
					String[] matrixs = matrix.split(" ");
					double[] pix_data = new double[matrixs.length];
					for (int j = 0; j < matrixs.length; j++) {
						pix_data[j] = Double.parseDouble(matrixs[j]);
					}
					trainData trainData = new trainData(pix_data, Integer.parseInt(dataset[1].replaceAll(",", "")));
					datas.add(trainData);
					combine = null;
					System.out.println(datas.size() + "/" + datacount + "  Memory Left:"
					        + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
				} else {
					combine += str;
				}

			}
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File file = new File(savePath);
		try {
			OutputStream os = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(os);
			objectOutputStream.writeObject(datas);
			objectOutputStream.close();
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
