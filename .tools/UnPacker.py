import cPickle
import numpy as np

f = open('./mnist.pkl', 'rb')
training_data, validation_data, test_data = cPickle.load(f)
f.close()
print('[')
for data in zip(training_data[0],training_data[1]):
    print(str(np.array(data[0]))+str(data[1])+",")
print(']')
